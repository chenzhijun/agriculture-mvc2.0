package com.alipay.config;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *版本：3.3
 *日期：2012-08-10
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
	
 *提示：如何获取安全校验码和合作身份者ID
 *1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
 *3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”

 *安全校验码查看时，输入支付密码后，页面呈灰色的现象，怎么办？
 *解决方法：
 *1、检查浏览器配置，不让浏览器做弹框屏蔽设置
 *2、更换浏览器或电脑，重新登录查询。
 */

public class AlipayConfig {
	
	//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	// 合作身份者ID，以2088开头由16位纯数字组成的字符串
	public static String partner = "2088911385004881";
	
	public static String seller_id = partner;
	
	// 收款支付宝账号
	public static String seller_email = "kain80817@163.com";
	// 商户的私钥
	public static String key = "11gphrjuo72mjqae78q0ezw005b3amxb";

	// 商户的私钥RSA
	public static String private_key = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAPn653yJE+f/UvjPiUWGRqCUAs+98HKwulsPThNzCGlMfM7vqbQ8HgIa6gzFj0M8kiMUOn0dQejBnz0FvHx4b6sBOviA0YW/yezaU44tQTsnvROVxY7QS618Cdeos8zHYszr9SWcnJGEuiRkdrKewSjHuKx4igltrpJ8IgKtDf/vAgMBAAECgYEA2ic9ZzAVYCww05QCHttcvXwNrA/9S4mxSBBmOZD08Tff35PFjNW99mZYhMbQDIRKoDRiCpfC0gZPGijsYc+U2nTQWxm5tZYub+9TaXJKkYu2wF6KLmb9qSlTC2/rz7M4MpnDv9cigd4cU1ePYcJvm20ibtB0A0RDzHuHkZvF0nECQQD+eF/HjoNj/gwv7iwDq437irFSaYqwRrMnyxwm5ucS2j0QKlTUwpztuEBJVZGFbp+p8hrYB+fUdY/tkvM79xZpAkEA+3uesJ6LImiX4+TjzeNGPVfjc0FjYlK+k/u/xLZKU8G6Bx56xgF07fVteQC5v0n+8hJpYhTRX2Vh5nSJ+iiIlwJBAMLSaAoBSwjAFJaRkzMQIfjmO1IsKZlB16Hst9X0zA679SouLpphkQHTIazDjVEJ28Wh7PlePBiGFTY/XmeXiZkCQFIDjcwx2FQ/YhGravORRC3rRL0ovP2q2IYwWukPPgw0qLN7qDR88yJLoQkOSIVu31sY0A9xlJqU2KmspfbUUz8CQEn/xIbibtDdfFLbnVYlLZgaZFgsAFoomQceft1TYm3tlSHuUuH5ARxLAQczj9eQlH+0uw6AsETDmgzRplulMII=";

	
	// 支付宝的公钥，无需修改该值
	public static String ali_public_key  = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";

	
	//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	

	// 调试用，创建TXT日志文件夹路径
	public static String log_path = "D:\\";

	// 字符编码格式 目前支持 gbk 或 utf-8
	public static String input_charset = "utf-8";
	
	// 签名方式 不需修改
	public static String sign_type = "RSA";

}
