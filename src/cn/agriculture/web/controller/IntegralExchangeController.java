package cn.agriculture.web.controller;

import java.sql.SQLException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.IntegralExchangeReportForm;
import cn.agriculture.web.service.IntegralExchangeReportService;

@Slf4j
@Controller("IntegralExchangeReportController")
@RequestMapping("/")
public class IntegralExchangeController {

    @Autowired
    IntegralExchangeReportService integralExchangeReportService;

    @RequestMapping(value = "initIntegralExchange", method = RequestMethod.GET)
    public String initAlipayReport(Model model) {
        log.info("积分兑换账单列表");
        model.addAttribute("list", integralExchangeReportService.search());
        return "manager/integralExchange/integralExchangeReportList";
    }

    @RequestMapping(value = "delIntegralExchangeReport", method = RequestMethod.GET)
    public String delIntegralExchangeReport(Model model, IntegralExchangeReportForm integralExchangeReportForm) throws SQLException {
        log.info("删除作废的积分兑换账单");
        boolean result = integralExchangeReportService.del(integralExchangeReportForm);
        if (!result) {
            throw new SQLException("删除作废的积分兑换账单失败！");
        }
        model.addAttribute("list", integralExchangeReportService.search());
        return "manager/integralExchange/integralExchangeReportList";
    }

    @RequestMapping(value = "deliverIntegralExchangeReport", method = RequestMethod.GET)
    public String deliverIntegralExchangeReport(Model model, IntegralExchangeReportForm integralExchangeReportForm) throws SQLException {
        log.info("发货积分兑换账单");
        boolean result = integralExchangeReportService.deliver(integralExchangeReportForm);
        if (!result) {
            throw new SQLException("发货积分兑换账单失败！");
        }
        model.addAttribute("list", integralExchangeReportService.search());
        return "manager/integralExchange/integralExchangeReportList";
    }
}
