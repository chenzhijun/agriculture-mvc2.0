package cn.agriculture.web.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.IntegralRuleForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.service.IntegralRuleService;

@Slf4j
@Controller("IntegralRuleController")
@RequestMapping("/")
public class IntegralRuleController {

    @Autowired
    IntegralRuleService integralRuleService;

    @RequestMapping(value = "initIntegralRule", method = RequestMethod.GET)
    public String initIntegralRule(Model model, IntegralRuleForm integralRuleForm) {
        log.info("积分规则设定初始化");
        model.addAttribute("list", integralRuleService.searchIntegralRuleList(integralRuleForm));
        return "manager/integralRule/integralRule";
    }

    @RequestMapping(value = "initAddIntegralRule", method = RequestMethod.GET)
    public String initAddIntegralRule(Model model, IntegralRuleForm integralRuleForm) {
        log.info("积分规则添加初始化");
        return "manager/integralRule/addIntegralRule";
    }

    @RequestMapping(value = "initUpdateIntegralRule", method = RequestMethod.GET)
    public String initUpdateIntegralRule(Model model, IntegralRuleForm integralRuleForm) {
        log.info("积分规则修改初始化");
        List<IntegralRuleForm> list = integralRuleService.searchIntegralRuleList(integralRuleForm);
        if (list != null && list.size() == 1) {
            model.addAttribute("integralRuleForm", list.get(0));
        }
        return "manager/integralRule/editIntegralRule";
    }

    @RequestMapping(value = "startIntegralRule", method = RequestMethod.GET)
    public String startIntegralRule(Model model, HttpSession session,
            IntegralRuleForm integralRuleForm) throws SQLException {
        log.info("启用积分规则初始化");
        UVO uvo = (UVO)session.getAttribute("UVO");
        integralRuleForm.setUpdateUser(uvo.getUserName());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        integralRuleForm.setUpdateTime(dateformat.format(date));
        integralRuleForm.setStatus("启用");
        boolean ret = integralRuleService.update(integralRuleForm);
        if (ret) {
            IntegralRuleForm search = new IntegralRuleForm();
            model.addAttribute("list", integralRuleService.searchIntegralRuleList(search));
        } else {
            throw new SQLException("启用积分规则失败！");
        }
        return "manager/integralRule/integralRule";
    }

    @RequestMapping(value = "stopIntegralRule", method = RequestMethod.GET)
    public String stopIntegralRule(Model model, HttpSession session,
            IntegralRuleForm integralRuleForm) throws SQLException {
        log.info("启用积分规则初始化");
        UVO uvo = (UVO)session.getAttribute("UVO");
        integralRuleForm.setUpdateUser(uvo.getUserName());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        integralRuleForm.setUpdateTime(dateformat.format(date));
        integralRuleForm.setStatus("停用");
        boolean ret = integralRuleService.update(integralRuleForm);
        if (ret) {
            IntegralRuleForm search = new IntegralRuleForm();
            model.addAttribute("list", integralRuleService.searchIntegralRuleList(search));
        } else {
            throw new SQLException("停用积分规则失败！");
        }
        return "manager/integralRule/integralRule";
    }

    @RequestMapping(value = "updateIntegralRule", method = RequestMethod.POST)
    public String updateIntegralRule(Model model, HttpSession session,
            @Valid @ModelAttribute("integralRuleForm") IntegralRuleForm integralRuleForm,
            BindingResult results) throws SQLException {
        log.info("积分规则修改");
        if (results.hasErrors()) {
            model.addAttribute("integralRuleForm", integralRuleForm);
            return "manager/integralRule/editIntegralRule";
        }
        UVO uvo = (UVO)session.getAttribute("UVO");
        integralRuleForm.setUpdateUser(uvo.getUserName());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        integralRuleForm.setUpdateTime(dateformat.format(date));
        boolean ret = integralRuleService.update(integralRuleForm);
        if (ret) {
            IntegralRuleForm search = new IntegralRuleForm();
            model.addAttribute("list", integralRuleService.searchIntegralRuleList(search));
        } else {
            throw new SQLException("修改积分规则失败！");
        }
        return "manager/integralRule/integralRule";
    }

    @RequestMapping(value = "addIntegralRule", method = RequestMethod.POST)
    public String addIntegralRule(Model model, HttpSession session,
            @Valid @ModelAttribute("integralRuleForm") IntegralRuleForm integralRuleForm,
            BindingResult results) throws SQLException {
        log.info("积分规则添加");
        if (results.hasErrors()) {
            model.addAttribute("integralRuleForm", integralRuleForm);
            return "manager/integralRule/addIntegralRule";
        }
        UVO uvo = (UVO)session.getAttribute("UVO");
        integralRuleForm.setUpdateUser(uvo.getUserName());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        integralRuleForm.setUpdateTime(dateformat.format(date));
        boolean ret = integralRuleService.add(integralRuleForm);
        if (ret) {
            IntegralRuleForm search = new IntegralRuleForm();
            model.addAttribute("list", integralRuleService.searchIntegralRuleList(search));
        } else {
            throw new SQLException("添加积分规则失败！");
        }
        return "manager/integralRule/integralRule";
    }

    @RequestMapping(value = "deleteIntegralRule", method = RequestMethod.GET)
    public String deleteIntegralRule(Model model, HttpSession session,
            @Valid @ModelAttribute("integralRuleForm") IntegralRuleForm integralRuleForm,
            BindingResult results) throws SQLException {
        log.info("积分规则删除");
        boolean ret = integralRuleService.delete(integralRuleForm);
        if (ret) {
            IntegralRuleForm search = new IntegralRuleForm();
            model.addAttribute("list", integralRuleService.searchIntegralRuleList(search));
        } else {
            throw new SQLException("删除积分规则失败！");
        }
        return "manager/integralRule/integralRule";
    }
}
