package cn.agriculture.web.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.DistributorGoodsService;
import cn.agriculture.web.service.GoodsService;

@Slf4j
@Controller("DistributorGoodsController")
@RequestMapping("/")
public class DistributorGoodsController {

	@Autowired
	DistributorGoodsService distributorGoodsService;

	@Autowired
	GoodsService goodsService;
	
	@Autowired
	CartService cartService;
	
    @RequestMapping(value = "initDistributorGoods", method = RequestMethod.GET)
    public String initDistributorGoods(Model model, HttpSession session, GoodsForm goodsForm, Device device) {
    	log.info("分销商商品列表初始化");
    	List<GoodsForm> commodityType = goodsService.getType();
    	int sum=commodityType.size();
		for(int i=1;i<=sum;i++) {
			commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
		}
    	model.addAttribute("goodsForm", goodsForm);
    	model.addAttribute("commodityType",commodityType);
    	UVO uvo = new UVO();
		session.setAttribute("UVO", uvo);
    	model.addAttribute("cartList", new ArrayList<CartForm>());
    	model.addAttribute("list", distributorGoodsService.searchDistributorGoodsList(goodsForm));
    	if(device.isNormal()) {
    	    return "shop/distributorGoods/distributorGoodsList";
    	} else {
    	    return "mobile/distributorGoods/distributorGoodsList";
    	}
    }
    
    @RequestMapping(value = "initDistributorGoodsDetail", method = RequestMethod.GET)
    public String initDistributorGoodsDetail(Model model, HttpSession session, GoodsForm goodsForm, Device device) {
    	log.info("分销商商品详细信息初始化");
    	List<GoodsForm> commodityType = goodsService.getType();
    	int sum=commodityType.size();
		for(int i=1;i<=sum;i++){
			commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
		}
    	model.addAttribute("commodityType",commodityType);
    	UVO uvo = (UVO)session.getAttribute("UVO");
    	if (uvo == null) {
    		uvo = new UVO();
    		session.setAttribute("UVO", uvo);
    	}
    	model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
    	model.addAttribute("cartList", new ArrayList<CartForm>());
    	GoodsForm distributorGoodsForm = distributorGoodsService.searchDistributorGoods(goodsForm);
    	distributorGoodsForm.setDistributorId(goodsForm.getDistributorId());
    	model.addAttribute("goodsForm", distributorGoodsForm);
    	if(device.isNormal()) {
    		return "shop/distributorGoods/distributorGoodsDetails";
    		
    	} else {
        	return "mobile/distributorGoods/distributorGoodsDetails";
    	}
    }
    
    @RequestMapping(value = "submitDistributor", method = RequestMethod.POST,params="AlipaySubmit")
	public String executeAlipaySubmit(Model model, HttpSession session, GoodsForm goodsForm, CartForm cartForm, Device device) throws SQLException {
    	log.info("由分销商直接推荐的商品销售页面初始化。");
		List<CartForm> cartList = new ArrayList<>();
		model.addAttribute("cartList", cartList);
		List<GoodsForm> commodityType = goodsService.getType();
		int sum=commodityType.size();
		for(int i=1;i<=sum;i++){
			commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
		}
    	model.addAttribute("goodsForm", goodsForm);
    	model.addAttribute("commodityType", commodityType);
		UVO uvo = new UVO();
		session.setAttribute("UVO", uvo);
		model.addAttribute("alipayForm", distributorGoodsService.searchDistributorAlipay(goodsForm));
		if(device.isNormal()) {
    		return "shop/alipay/distributorAlipayConfirm";
    	} else {
        	return "mobile/alipay/distributorAlipayConfirm";
    	}
	}
}
