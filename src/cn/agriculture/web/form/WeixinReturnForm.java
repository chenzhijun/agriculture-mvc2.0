package cn.agriculture.web.form;

import lombok.Data;

@Data
public class WeixinReturnForm {
    private String return_code;
    private String return_msg;
}
