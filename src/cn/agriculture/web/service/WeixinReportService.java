
package cn.agriculture.web.service;

import java.util.List;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.agriculture.web.form.WeixinReportForm;

@Service
public class WeixinReportService {

	@Autowired
	QueryDAO queryDao;
	
	@Autowired
	UpdateDAO updateDao;
	
	public List<WeixinReportForm> searchWeixinReportList() {
		List<WeixinReportForm> result = queryDao.executeForObjectList("WeixinReport.selectWeixinReportList", null);
		return result;
	}
	
	public boolean delWeixinReport(WeixinReportForm frm) {
		
		int result = updateDao.execute("WeixinReport.deleteWeixinReport", frm);
		if (result == 1) {
			return true;
		}
		return false;
	}

	public boolean editPayWeixinReport(WeixinReportForm frm) {
		
		int result = updateDao.execute("WeixinReport.editPayWeixinReport", frm);
		if (result == 1) {
			return true;
		}
		return false;
	}
	
	public boolean editFaWeixinReport(WeixinReportForm frm) {
		
		int result = updateDao.execute("WeixinReport.editFaWeixinReport", frm);
		if (result == 1) {
			return true;
		}
		return false;
	}
}

