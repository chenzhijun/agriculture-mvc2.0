package cn.agriculture.web.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.StringUtils;

import cn.agriculture.web.form.AlipayForm;
import cn.agriculture.web.form.DistributorPriceForm;
import cn.agriculture.web.form.GuestForm;
import cn.agriculture.web.form.IntegralRuleForm;
import cn.agriculture.web.form.ReturnForm;

@Service
@PropertySource("classpath:system.properties")
public class AlipayService {

    @Autowired
    QueryDAO queryDao;

    @Autowired
    UpdateDAO updateDao;

    @Autowired
    private Environment env;

    public AlipayForm searchAlipay(AlipayForm frm) {
        DistributorPriceForm distributorPriceForm = queryDao.executeForObject("Alipay.selectDistributorPrice", frm, DistributorPriceForm.class);
        AlipayForm alipayForm = new AlipayForm();
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");

        alipayForm.setOutTradeNo(distributorPriceForm.getDistributorId() + dateformat.format(date));
        alipayForm.setSubject(distributorPriceForm.getDistributorId() +"推荐的商品订单");
        Double price = Double.valueOf(distributorPriceForm.getRetailPrice());
        // 分销商销售不论是否满100都加8元邮费
        //if (price < 100) {
        price = price + 8;
        //}
        alipayForm.setPrice(String.valueOf(price));
        alipayForm.setBody(distributorPriceForm.getCommodityName());
        String host = env.getProperty("host.mobile");
        alipayForm.setShowUrl(host + "/initDistributorAlipayComfirm?distributorPriceId=" + distributorPriceForm.getDistributorPriceId());
        //alipayForm.setShowUrl("http://localhost:8080/agriculture-mvc/initDistributorAlipayComfirm?distributorPriceId=" + distributorPriceForm.getDistributorPriceId());
        alipayForm.setGuestId(distributorPriceForm.getDistributorId());
        alipayForm.setCommodityId(distributorPriceForm.getCommodityId());
        alipayForm.setDistributorName(distributorPriceForm.getDistributorName());
        alipayForm.setDistributorPriceId(distributorPriceForm.getDistributorPriceId());
        return alipayForm;
    }

    public boolean editStock(AlipayForm frm) {
        Integer stock = queryDao.executeForObject("Alipay.selectStock", frm, Integer.class);
        if (stock < Integer.valueOf(frm.getCount())) {
            return false;
        }
        int result = updateDao.execute("Alipay.editStock", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

    public boolean editPayment(ReturnForm frm) {
        if ("TRADE_SUCCESS".equals(frm.getTrade_status())) {
            int result = updateDao.execute("Alipay.editPayment", frm);
            if (result == 1) {
            	// 根据客户花费钱数增加积分
                setIntegral(frm);
                return true;
            }
        }
        return false;
    }

    private void setIntegral(ReturnForm frm) {
        IntegralRuleForm ifrm = new IntegralRuleForm();
        ifrm.setType("下单");
        ifrm.setStatus("启用");
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        ifrm.setStartDate(dateformat.format(date));
        IntegralRuleForm ret =
                queryDao.executeForObject("IntegralRule.selectIntegralRule",
                                          ifrm,
                                          IntegralRuleForm.class);
        if (ret != null && !StringUtils.isNullOrEmpty(ret.getHowScore())) {
            AlipayForm alipayForm = new AlipayForm();
            alipayForm.setOutTradeNo(frm.getOut_trade_no());
            AlipayForm alipayHistory = queryDao.executeForObject("Alipay.selectPrice", alipayForm, AlipayForm.class);
            BigDecimal price = new BigDecimal(alipayHistory.getPrice());
            // 示例： 用户设置积分规则10元积5分，用户消费9元。
            // 四舍五入算法：5*9/10=4.5 结果积分为5。
            // 阶梯去尾算法：5*9/10=4.5 结果积分为4。
            // 满额积分算法：未满足10元，结果积分为0。
            BigDecimal howMoney = new BigDecimal(ret.getHowMoney());
            BigDecimal howScore = new BigDecimal(ret.getHowScore());
            BigDecimal point = new BigDecimal("0");
            //大于
            if (howMoney.compareTo(BigDecimal.ZERO) == 1) {
                if ("四舍五入".equals(ret.getHowSet())) {
                    point = price.multiply(howScore.divide(howMoney));
                    point = point.setScale(0,BigDecimal.ROUND_HALF_UP);
                } else if ("阶梯去尾".equals(ret.getHowSet())) {
                    point = price.multiply(howScore.divide(howMoney));
                    point = new BigDecimal(point.intValue());
                } else if ("满额积分".equals(ret.getHowSet())) {
                    point = howScore.multiply(new BigDecimal(price.divide(howMoney).intValue()));
                }
            }
            GuestForm guest = new GuestForm();
            guest.setGuestId(alipayHistory.getGuestId());
            GuestForm guestPointForm = queryDao.executeForObject("Guest.selectGuestPoint", guest, GuestForm.class);
            BigDecimal guestPoint = new BigDecimal(0);
            if (!StringUtils.isNullOrEmpty(guestPointForm.getGuestPoint())) {
                guestPoint = new BigDecimal(guestPointForm.getGuestPoint());
            }
            guest.setPoint((point.add(guestPoint)) + "");
            updateDao.execute("Guest.editGuestPoint", guest);
        }
    }
}
