package cn.agriculture.web.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import cn.agriculture.web.form.AlipayForm;
import cn.agriculture.web.form.DistributorPriceForm;
import cn.agriculture.web.form.GoodsForm;

@Service
@PropertySource("classpath:system.properties")
public class DistributorGoodsService {
	
	@Autowired
	QueryDAO queryDao;
	
	@Autowired
	UpdateDAO updateDao;
	
	@Autowired
	private Environment env;
	
	public List<GoodsForm> searchDistributorGoodsList(GoodsForm frm) {
		List<GoodsForm> result = queryDao.executeForObjectList("DistributorGoods.selectDistributorGoodsList", frm);
		return result;
	}
	
	public GoodsForm searchDistributorGoods(GoodsForm frm) {
		GoodsForm result = queryDao.executeForObject("DistributorGoods.selectDistributorGoods", frm, GoodsForm.class);
		return result;
	}
	
	public AlipayForm searchDistributorAlipay(GoodsForm frm) {
		DistributorPriceForm distributorPriceForm = queryDao.executeForObject("DistributorGoods.selectDistributorPrice", frm, DistributorPriceForm.class);
        AlipayForm alipayForm = new AlipayForm();
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");

        alipayForm.setOutTradeNo(distributorPriceForm.getDistributorId() + dateformat.format(date));
        alipayForm.setSubject(distributorPriceForm.getDistributorId() +"推荐的商品订单");
        Double price = Double.valueOf(frm.getCount()) * Double.valueOf(distributorPriceForm.getRetailPrice());
        // 分销商销售不论是否满100都加8元邮费
        //if (price < 100) {
        price = price + 8;
        //}
        BigDecimal sumAmount = new BigDecimal(price);
        String finPrice = String.valueOf(String.valueOf(sumAmount.setScale(2, BigDecimal.ROUND_HALF_UP)));
        alipayForm.setPrice(finPrice);
        alipayForm.setCount(frm.getCount());
        alipayForm.setBody("品名：" + distributorPriceForm.getCommodityName() +", 数量："+ frm.getCount() +", 总价："+ finPrice + "(该订单优惠力度太大挣不回邮费，所以加收您邮费8元)");
        String host = env.getProperty("host.mobile");
        alipayForm.setShowUrl(host + "/initDistributorAlipayComfirm?distributorPriceId=" + distributorPriceForm.getDistributorPriceId());
        //alipayForm.setShowUrl("http://localhost:8080/agriculture-mvc/initDistributorAlipayComfirm?distributorPriceId=" + distributorPriceForm.getDistributorPriceId());
        alipayForm.setGuestId(distributorPriceForm.getDistributorId());
        alipayForm.setCommodityId(distributorPriceForm.getCommodityId());
        alipayForm.setDistributorName(distributorPriceForm.getDistributorName());
        alipayForm.setDistributorPriceId(distributorPriceForm.getDistributorPriceId());
        return alipayForm;
	}
}
