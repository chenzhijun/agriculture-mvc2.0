package cn.agriculture.web.weixin.controller;

import java.sql.SQLException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.WeixinReportForm;
import cn.agriculture.web.service.WeixinReportService;

@Slf4j
@Controller("WeixinReportController")
@RequestMapping("/")
public class WeixinReportController {

    @Autowired
    WeixinReportService weixinReportService;

    @RequestMapping(value = "initWeixinReport", method = RequestMethod.GET)
    public String initWeixinReport(Model model) {
        log.info("检索微信账单列表");
        model.addAttribute("list", weixinReportService.searchWeixinReportList());
        return "manager/report/weixinReportList";
    }

    @RequestMapping(value = "delWeixinReport", method = RequestMethod.GET)
    public String initWeixinReport(Model model, WeixinReportForm weixinReportForm) throws SQLException {
        log.info("删除作废的微信账单");
        boolean result = weixinReportService.delWeixinReport(weixinReportForm);
        if (!result) {
            throw new SQLException("删除作废的微信账单失败！");
        }
        model.addAttribute("list", weixinReportService.searchWeixinReportList());
        return "manager/report/weixinReportList";
    }

    @RequestMapping(value = "editPayWeixinReport", method = RequestMethod.GET)
    public String init1WeixinReport(Model model, WeixinReportForm weixinReportForm) throws SQLException {
        log.info("修改付款状态");
        weixinReportForm.setState("已支付");
        boolean result = weixinReportService.editPayWeixinReport(weixinReportForm);
        if(!result) {
            throw new SQLException("修改支付状态失败！");
        }
        model.addAttribute("list", weixinReportService.searchWeixinReportList());
        return "manager/report/weixinReportList";
    }

    @RequestMapping(value = "editFaWeixinReport", method = RequestMethod.GET)
    public String init2WeixinReport(Model model, WeixinReportForm weixinReportForm) throws SQLException  {
        log.info("修改发货状态");
        weixinReportForm.setState("已发货");
        boolean result = weixinReportService.editFaWeixinReport(weixinReportForm);
        if(!result) {
            throw new SQLException("修改发货状态失败！");
        }
        model.addAttribute("list", weixinReportService.searchWeixinReportList());
        return "manager/report/weixinReportList";
    }
}
