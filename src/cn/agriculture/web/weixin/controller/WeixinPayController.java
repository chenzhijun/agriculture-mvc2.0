package cn.agriculture.web.weixin.controller;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.GuestForm;
import cn.agriculture.web.form.ReceiveForm;
import cn.agriculture.web.form.ReturnForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.form.WeixinReturnForm;
import cn.agriculture.web.form.WeixinpayForm;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.GoodsService;
import cn.agriculture.web.service.GuestService;
import cn.agriculture.web.service.ReceiveService;
import cn.agriculture.web.weixin.service.WeixinCartService;
import cn.agriculture.web.weixin.service.WeixinPayService;

import com.mysql.jdbc.StringUtils;
import com.wxpay.utils.GetWxOrderno;

@Slf4j
@Controller("WeixinPayController")
@RequestMapping("/")
@PropertySource("classpath:system.properties")
public class WeixinPayController {

    @Autowired
    CartService cartService;
    @Autowired
    GuestService guestService;
    @Autowired
    GoodsService goodsService;
    @Autowired
    WeixinPayService weixinPayService;
    @Autowired
    ReceiveService receiveService;
    @Autowired
    WeixinCartService weixinCartService;

    @RequestMapping(value = "guestWeixinPaySubmit", method = RequestMethod.POST)
    public String executeGuestWeixinPaySubmit(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute("weixinPayForm") WeixinpayForm weixinPayForm, BindingResult results, Device device) throws SQLException {
        log.info("由匿名用户购买商品向微信发起支付请求。");
        List<CartForm> cartList = new ArrayList<>();
        model.addAttribute("cartList", cartList);
        GoodsForm goodsForm = new GoodsForm();
        goodsForm.setCommodityId(weixinPayForm.getCommodityId());
        model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));

        List<GoodsForm> commodityType = goodsService.getType();

        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType", commodityType);
        if (results.hasErrors()) {
            log.info("内容验证出错");
            return "weixin/weixinPay/guestWeixinPayConfirm";
        }
        weixinPayForm.setUpdateUser(weixinPayForm.getGuestId());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        weixinPayForm.setUpdateTime(dateformat.format(date));
        String tempCommodityId = weixinPayForm.getCommodityId();
        weixinPayForm.setCommodityId(null);
        // 添加订单信息
        boolean hisResult = weixinCartService.addWeixinpayHistory(weixinPayForm);
        if(!hisResult) {
            throw new SQLException("添加账单失败！");
        }
        weixinPayForm.setCommodityId(tempCommodityId);
        boolean result = weixinPayService.editStock(weixinPayForm);
        if(!result) {
            model.addAttribute("message", "该商品刚刚被卖完了！");
            return "weixin/weixinPay/guestWeixinPayConfirm";
        }
        // 取得OpenId
        String openId = (String) session.getAttribute("WEIXIN_OPENID");
        // 微信支付
        boolean payResult = weixinPayService.getWxPayResult(model, session, request, response ,weixinPayForm);
        if (!payResult) {
            model.addAttribute("message", "微信实付失败！");
            return "weixin/weixinPay/guestWeixinPayConfirm";
        }
        model.addAttribute("openId", openId);
        return "weixin/weixinPay";
    }

    @RequestMapping(value = "weixinpaySubmit", method = RequestMethod.POST , params="WeiXinGo")
    public String executeWeixinpaySubmit(Model model, HttpSession session,HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute("weixinpayForm") WeixinpayForm weixinpayForm, BindingResult results, Device device, ReceiveService receiveservice) throws SQLException {
        log.info("修改购物车信息为已付款");
        UVO uvo = (UVO)session.getAttribute("UVO");
        if (uvo == null || StringUtils.isNullOrEmpty(uvo.getGuestId())) {
            return "redirect:/initGuestLogin";
        }
        GoodsForm goodsForm=new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType", commodityType);
        weixinpayForm.setUpdateUser(uvo.getGuestName());
        ReceiveForm receiveForm=new ReceiveForm();
        receiveForm.setAddressId(weixinpayForm.getAddressId());
        receiveForm=receiveService.searchReceive(receiveForm);
        if(receiveForm==null){
            CartForm cartForm = new CartForm();
            cartForm.setGuestId(uvo.getGuestId());
            model.addAttribute("cartList", cartService.searchCartList(cartForm));
            model.addAttribute("message", "请添加一个您的送货地址！");
            return "weixin/cart/cart-3";
        }
        weixinpayForm.setReceiveAddress(receiveForm.getAddressName());
        weixinpayForm.setReceiveMobile(receiveForm.getMobile());
        weixinpayForm.setReceiveName(receiveForm.getReceiveName());
        weixinpayForm.setReceivePhone(receiveForm.getPhone());
        weixinpayForm.setReceiveZip(receiveForm.getPost());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        weixinpayForm.setUpdateTime(dateformat.format(date));
        weixinpayForm.setGuestId(uvo.getGuestId());
        model.addAttribute("WeixinpayForm", weixinpayForm);
        boolean hisResult = weixinCartService.addWeixinpayHistory(weixinpayForm);
        if(!hisResult) {
            throw new SQLException("添加微信宝账单失败！");
        }
        CartForm cartForm = new CartForm();
        cartForm.setUpdateUser(uvo.getGuestName());
        cartForm.setUpdateTime(dateformat.format(date));
        cartForm.setGuestId(uvo.getGuestId());
        if(weixinpayForm.getCartFormList()!=null){
        int b = weixinpayForm.getCartFormList().size();
        for (int k = 0; k < b;k++) {
            cartForm.setCartId(weixinpayForm.getCartFormList().get(k).getCartId());
            boolean result = cartService.editStatus(cartForm);
            if(!result) {
                throw new SQLException("修改微信支付状态失败！");
                }
            }
        }
        if (!"".equals((weixinpayForm.getSumPoint()))) {
            // 用户积分更新
            GuestForm guestForm = new GuestForm();
            guestForm.setGuestId(weixinpayForm.getGuestId());
            String guestPoint = guestService.searchGuestPoint(guestForm).getGuestPoint();
            guestForm.setGuestPoint(String.valueOf(Double.valueOf(guestPoint) - Double.valueOf(weixinpayForm.getSumPoint())));
            boolean result = guestService.editPoint(guestForm);
            if(!result) {
                throw new SQLException("用户积分更新失败！");
            }
            CartForm cartFormPoint = new CartForm();
            cartFormPoint.setGuestId(weixinpayForm.getGuestId());
            model.addAttribute("cartList", cartService.searchCartList(cartFormPoint));
            String out_trade_no = weixinpayForm.getOutTradeNo();
            ReturnForm returnForm = new ReturnForm();
            returnForm.setOut_trade_no(out_trade_no);
            model.addAttribute("returnForm", returnForm);
            return "weixin/cart/cart-3";
        } else {
            model.addAttribute("cartList", cartService.searchCartList(cartForm));
            model.addAttribute("alipayForm", weixinpayForm);
            // 取得OpenId
            String openId = (String) session.getAttribute("WEIXIN_OPENID");
            // 微信支付
            boolean payResult = weixinPayService.getWxPayResult(model, session, request, response ,weixinpayForm);
            if (!payResult) {
                model.addAttribute("message", "微信支付失败！");
                return "weixin/cart/cart-2";
            }
            model.addAttribute("openId", openId);
            return "weixin/weixinPay";
        }

    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "initWeixinPayBack", method = RequestMethod.POST)
    public void executeInitReturn(Model model, WeixinReturnForm returnForm, HttpServletResponse response) throws Exception {
        log.info("这是一个微信支付主动调用商家网站信息的日志");
        log.info(returnForm.getReturn_code());
        log.info(returnForm.getReturn_msg());
        PrintWriter out = response.getWriter();
        if (!"SUCCESS".equals(returnForm.getReturn_code())) {
            log.info("fail");
            String xml="<xml>"+
                    "<return_code><![CDATA[FAIL]]</appid>"+
                    "<return_msg><![CDATA[微信返回失败]]></return_msg>";
            out.print(xml);
            return;
        }
        log.info("success");
        Map<String, String> params = GetWxOrderno.doXMLParse(returnForm.getReturn_msg());
        boolean result = weixinPayService.editPayment(params);
        if (!result) {
            String xml="<xml>"+
                    "<return_code><![CDATA[FAIL]]</appid>"+
                    "<return_msg><![CDATA[计算积分]]></return_msg>";
            out.print(xml);
            return;
        }
        String xml="<xml>"+
                "<return_code><![CDATA[SUCCESS]]</appid>"+
                "<return_msg><![CDATA[OK]]></return_msg>";
        response.getOutputStream().write(xml.getBytes());
        return;
    }
}
