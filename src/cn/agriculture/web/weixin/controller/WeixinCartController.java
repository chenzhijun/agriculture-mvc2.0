package cn.agriculture.web.weixin.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.AlipayForm;
import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.GuestForm;
import cn.agriculture.web.form.ListBean;
import cn.agriculture.web.form.ReceiveForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.GoodsService;
import cn.agriculture.web.service.GuestService;
import cn.agriculture.web.service.ReceiveService;
import cn.agriculture.web.weixin.service.WeixinCartService;

@Slf4j
@Controller("WeixinCartController")
@RequestMapping("/")
@PropertySource("classpath:system.properties")
public class WeixinCartController {

    @Autowired
    CartService cartService;

    @Autowired
    WeixinCartService weixinCartService;

    @Autowired
    GuestService guestService;

    @Autowired
    GoodsService goodsService;

    @Autowired
    ReceiveService receiveservice;

    @Autowired
    private Environment env;

    @RequestMapping(value = "weixinConfirm", method = RequestMethod.POST,params="delChoosedCart")
    public String weixinexecuteDelChoosedCart(Model model, HttpSession session,
            CartForm cartForm, Device device) throws SQLException {
        log.info("删除选中购物车");
        UVO uvo = (UVO) session.getAttribute("UVO");
        if (uvo == null || StringUtils.isEmpty(uvo.getGuestId())) {
            return "redirect:/initGuestLogin";
        }
        List<ListBean> listBean = cartForm.getListBean();
        if (listBean != null) {
            int b = listBean.size();
            for (int k = 0; k < b;k++) {
                String check = listBean.get(k).getCheckArray();
                if (check != null) {
                    cartForm.setCount(listBean.get(k).getCountArray());
                    cartForm.setCommodityId(listBean.get(k).getCommodityId());
                    cartForm.setCartId(listBean.get(k).getCartId());
                    cartForm.setUpdateUser(uvo.getGuestName());
                    Date date = new Date();
                    SimpleDateFormat dateformat = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");
                    cartForm.setUpdateTime(dateformat.format(date));

                    boolean result = cartService.delCart(cartForm);
                    if (!result) {
                        throw new SQLException("删除购物车失败！");
                    }
                }
            }
        }

        cartForm.setGuestId(uvo.getGuestId());
        GoodsForm goodsForm = new GoodsForm();
        // goodsForm.setType("粮食");
        // model.addAttribute("goodsForm", goodsForm);
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        model.addAttribute("cartList", cartService.searchCartList(cartForm));
        model.addAttribute("list",cartService.searchAlipayHistoryList(cartForm));
        cartForm.setGuestId(uvo.getGuestId());
        List<CartForm> cartFormList=cartService.searchCartList(cartForm);
        for(int i=0;i<cartFormList.size();i++){
            BigDecimal smallSumPrice=new BigDecimal(Double.toString(Double.valueOf(cartFormList.get(i).getCount())*Double.valueOf(cartFormList.get(i).getRetailPrice())));
            cartFormList.get(i).setSmallSumPrice(String.valueOf(smallSumPrice.setScale(2, BigDecimal.ROUND_HALF_UP)));
        }
        model.addAttribute("cartList", cartFormList);
        return "weixin/cart/cart-1";
    }

    @RequestMapping(value = "weixinConfirm", method = RequestMethod.POST,params="Go")
    public String weixinConfirm(Model model, HttpSession session,
            CartForm cartForm, Device device) throws SQLException {
        GoodsForm goodsForm = new GoodsForm();
        // goodsForm.setType("粮食");
        // model.addAttribute("goodsForm", goodsForm);
        List<GoodsForm> commodityType = goodsService.getType();
        int sum1=commodityType.size();
        for(int i=1;i<=sum1;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        log.info("确认支付");
        // CartForm cartForm = new CartForm();
        UVO uvo = (UVO) session.getAttribute("UVO");
        if (uvo == null || StringUtils.isEmpty(uvo.getGuestId())) {
            return "redirect:/initGuestLogin";
        }

        List<ListBean> listBean = cartForm.getListBean();
        if(listBean==null)
        {
            cartForm = new CartForm();
            cartForm.setGuestId(uvo.getGuestId());
            List<CartForm> cartFormList=cartService.searchCartList(cartForm);
            model.addAttribute("cartForm",cartForm);
            model.addAttribute("cartList", cartFormList);
            model.addAttribute("list", cartService.searchAlipayHistoryList(cartForm));
            model.addAttribute("message", "对不起，您的购物车为空，请选择商品结算");
            if(device.isNormal()) {
            return "weixin/cart/cart-1";
        } else {
            return "weixin/cart/cart-1";
        }
        }
        int b = listBean.size();
        String cartIds = "";
        for (int i = 0; i < b; i++) {
            cartForm.setUpdateUser(uvo.getGuestName());
            Date date = new Date();
            SimpleDateFormat dateformat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            cartForm.setUpdateTime(dateformat.format(date));
            cartForm.setCommodityId(listBean.get(i).getCommodityId());
            cartForm.setCount(listBean.get(i).getCountArray());
            cartForm.setGuestId(uvo.getGuestId());
            String check = listBean.get(i).getCheckArray();
            if (check != null) {
                cartForm.setCartId(listBean.get(i).getCartId());
                boolean result = cartService.editStockByCart(cartForm);
                if (!result) {
                    model.addAttribute("message", "库存不足");
                    CartForm cartForm1 = new CartForm();
                    cartForm1.setGuestId(uvo.getGuestId());
                    List<CartForm> cartFormList=cartService.searchCartList(cartForm1);
                    for(int n=0;n<cartFormList.size();n++){
                        BigDecimal smallSumPrice=new BigDecimal(Double.toString(Double.valueOf(cartFormList.get(n).getCount())*Double.valueOf(cartFormList.get(n).getRetailPrice())));
                        cartFormList.get(n).setSmallSumPrice(String.valueOf(smallSumPrice.setScale(2, BigDecimal.ROUND_HALF_UP)));
                    }
                    model.addAttribute("cartForm",cartForm);
                    model.addAttribute("cartList", cartFormList);
                    return "wexin/cart/cart-1";

                }
                boolean hisResult = cartService.updateCart(cartForm);
                if (!hisResult) {
                    throw new SQLException("添加支付宝失败");
                }
                cartIds = cartIds + ",'" + listBean.get(i).getCartId() + "'";
            }

        }
        if("".equals(cartIds)) {
            cartForm = new CartForm();
            cartForm.setGuestId(uvo.getGuestId());
            List<CartForm> cartFormList=cartService.searchCartList(cartForm);
            BigDecimal sum=new BigDecimal(0);
            for(int i=0;i<cartFormList.size();i++){
                BigDecimal smallSumPrice=new BigDecimal(Double.toString(Double.valueOf(cartFormList.get(i).getCount())*Double.valueOf(cartFormList.get(i).getRetailPrice())));
                cartFormList.get(i).setSmallSumPrice(String.valueOf(smallSumPrice.setScale(2, BigDecimal.ROUND_HALF_UP)));
                sum.add(smallSumPrice);
            }
            cartForm.setSumPrice(String.valueOf(sum));
            model.addAttribute("cartForm",cartForm);
            model.addAttribute("cartList", cartFormList);
            model.addAttribute("list", cartService.searchAlipayHistoryList(cartForm));
            model.addAttribute("message", "对不起，您未选择购买任何商品，请选择后购买");
            return "weixin/cart/cart-1";
        }
        cartForm.setCartId(cartIds.substring(1));
        // cartForm.setGuestId(uvo.getGuestId());
        List<CartForm> list = cartService.searchCartListForCartId(cartForm);
        AlipayForm alipayForm = new AlipayForm();
        String body = "您购买的商品如下：";
        Double price = 0d;
        for (CartForm item : list) {
            body = body
                    + "品名："
                    + item.getCommodityName()
                    + ", 数量："
                    + item.getCount()
                    + ", 总价："
                    + String.valueOf(Double.valueOf(item.getCount())
                            * Double.valueOf(item.getRetailPrice())) + ";";
            price = price + Double.valueOf(item.getCount())
                    * Double.valueOf(item.getRetailPrice());
        }
        alipayForm.setBody(body);
        alipayForm.setOutTradeNo(list.get(0).getCartId());
        // 不满100元加8元邮费
        if (price < 100) {
            price = price + 8;
            body = body + "(由于本次订单未满100元，加收您邮费8元)";
        }
        alipayForm.setCartFormList(list);
        alipayForm.setPrice(price.toString());
        GuestForm guestForm=new GuestForm();
        guestForm.setGuestId(uvo.getGuestId());
        String addressDefault=guestService.searchAddressId(guestForm).getAddressId();
        model.addAttribute("addressDefault", addressDefault);
        ReceiveForm receiveForm=new ReceiveForm();
        receiveForm.setGuestId(uvo.getGuestId());
        List<ReceiveForm> list1=receiveservice.searchlist(receiveForm);
        model.addAttribute("list",list1);
        String host = env.getProperty("host.web");
        alipayForm.setShowUrl(host + "/initCart");
        alipayForm.setSubject(body);
        model.addAttribute("alipayForm", alipayForm);
        cartForm.setGuestId(uvo.getGuestId());
        model.addAttribute("cartList", cartService.searchCartList(cartForm));
        return "weixin/cart/cart-2";

    }

    @RequestMapping(value = "weixinOrder", method = RequestMethod.GET)
    public String order(Model model, AlipayForm alipayForm, HttpSession session, Device device) {
        GoodsForm goodsForm=new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        log.info("我的订单");
        CartForm cartForm = new CartForm();
        model.addAttribute("cartForm", cartForm);
        UVO uvo = (UVO)session.getAttribute("UVO");
        if (uvo == null || StringUtils.isEmpty(uvo.getGuestId())) {
            return "redirect:/initGuestLogin";
        }
        cartForm.setGuestId(uvo.getGuestId());
        model.addAttribute("cartList", cartService.searchCartList(cartForm));
        model.addAttribute("pagesList", weixinCartService.searchWeixinOrderListCount(cartForm));
        model.addAttribute("orderList", weixinCartService.searchWeixinOrderList(cartForm, alipayForm.getIndex()));
        model.addAttribute("alipayForm", alipayForm);
           return "weixin/order";
    }

    @RequestMapping(value = "deleteWeixinOrder", method = RequestMethod.GET)
    public String deleteOrder(Model model, AlipayForm alipayForm, HttpSession session, Device device) {
        GoodsForm goodsForm=new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        int sum=commodityType.size();
        for(int i=1;i<=sum;i++){
            commodityType.get(i-1).setPh("ico-nav"+" "+"ico-nav-"+(i%7+1));
        }
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType",commodityType);
        log.info("删除我的订单");
        CartForm cartForm = new CartForm();
        model.addAttribute("cartForm", cartForm);
        UVO uvo = (UVO)session.getAttribute("UVO");
        if (uvo == null || StringUtils.isEmpty(uvo.getGuestId())) {
            return "redirect:/initGuestLogin";
        }
        cartForm.setGuestId(uvo.getGuestId());
        weixinCartService.deleteWeixinOrder(alipayForm);
        model.addAttribute("cartList", cartService.searchCartList(cartForm));
        model.addAttribute("pagesList", weixinCartService.searchWeixinOrderListCount(cartForm));
        model.addAttribute("orderList", weixinCartService.searchWeixinOrderList(cartForm, 0));
        return "weixin/order";
    }
}
