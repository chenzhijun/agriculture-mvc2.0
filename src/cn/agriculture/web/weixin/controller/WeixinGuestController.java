package cn.agriculture.web.weixin.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.GoodsForm;
import cn.agriculture.web.form.GuestForm;
import cn.agriculture.web.form.ReceiveForm;
import cn.agriculture.web.form.UVO;
import cn.agriculture.web.form.WeixinpayForm;
import cn.agriculture.web.service.CartService;
import cn.agriculture.web.service.CommodityTypeService;
import cn.agriculture.web.service.GoodsService;
import cn.agriculture.web.service.GuestService;
import cn.agriculture.web.service.ReceiveService;
import cn.agriculture.web.weixin.service.WeixinCartService;
import cn.agriculture.web.weixin.service.WeixinGuestService;

@Slf4j
@Controller("WeixinGuestController")
@RequestMapping("/")
public class WeixinGuestController {

    @Autowired
    GuestService guestService;

    @Autowired
    WeixinGuestService weixinGuestService;

    @Autowired
    CommodityTypeService commodityTypeService;

    @Autowired
    GoodsService goodsService;

    @Autowired
    CartService cartService;

    @Autowired
    WeixinCartService weixinCartService;

    @Autowired
    ReceiveService receiveservice;

    @RequestMapping(value = "weixinGuestAdd", method = RequestMethod.POST)
    public String weixinGuestAdd(Model model,
            HttpSession session,
            String retWeixin,
            @Valid @ModelAttribute("guestForm") GuestForm guestForm,
            BindingResult results,
            Device device) throws SQLException {
        model.addAttribute("guestForm", guestForm);
        GoodsForm goodsForm = new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType", commodityType);
        String passWord=guestForm.getPassword();
        String passWordConfirm=guestForm.getPasswordConfirm();
        if (!passWord.equals(passWordConfirm)){
            model.addAttribute("message1", "密码和确认密码必须一致！");
        }
        if (results.hasErrors()) {
            log.info("内容验证出错");
            List<CartForm> cartList = new ArrayList<>();
            model.addAttribute("cartList", cartList);
            model.addAttribute("ret", retWeixin);
            return "weixin/register-1";
        }
        if(guestForm.getGuestId().length() > 4 && "Guest".equals(guestForm.getGuestId().substring(0, 5))) {
            log.info("ID验证出错");
            model.addAttribute("message", "Guest是系统预留关键字，请避免使用！");
            List<CartForm> cartList = new ArrayList<>();
            model.addAttribute("cartList", cartList);
            model.addAttribute("ret", retWeixin);
            return "weixin/register-1";
        }
        if (!guestForm.getPassword().equals(guestForm.getPasswordConfirm())) {
            log.info("密码验证出错");
            model.addAttribute("message1", "密码和密码确认必须一致！");
            List<CartForm> cartList = new ArrayList<>();
            model.addAttribute("cartList", cartList);
            model.addAttribute("ret", retWeixin);
            return "weixin/register-1";
        }
        log.info("添加客户信息");
        guestForm.setUpdateUser(guestForm.getGuestId());
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        guestForm.setUpdateTime(dateformat.format(date));
        guestForm.setPoint(guestService.getIntegral());
        String openId = (String)session.getAttribute("WEIXIN_OPENID");
        guestForm.setOpenId(openId);
        boolean result = weixinGuestService.addGuest(guestForm);
        if (!result) {
            model.addAttribute("message", "该用户ID已被占用，请更换用户ID！");
            List<CartForm> cartList = new ArrayList<>();
            model.addAttribute("cartList", cartList);
            model.addAttribute("ret", retWeixin);
            return "weixin/register-1";
        }

        // 加入购物车的逻辑
        UVO uvo = new UVO();
        uvo.setGuestId(guestForm.getGuestId());
        uvo.setGuestName(guestForm.getGuestName());
        uvo.setPassword(guestForm.getPassword());
        uvo.setGender(guestForm.getGender());
        uvo.setEmail(guestForm.getEmail());
        uvo.setMobile(guestForm.getMobile());
        uvo.setQq(guestForm.getQq());
        uvo.setPhone(guestForm.getPhone());
        uvo.setZip(guestForm.getZip());
        uvo.setPoint(guestForm.getPoint());
        session.setAttribute("UVO", uvo);

        CartForm cartForm = (CartForm) session.getAttribute("WEIXIN_CARTFORM");
        cartForm.setGuestId(uvo.getGuestId());
        //设置updateTime
        cartForm.setUpdateTime(dateformat.format(date));
        //设置updateUser
        cartForm.setUpdateUser(uvo.getGuestName());
        List<CartForm> cartFormList = cartService.searchCartList(cartForm);
        GoodsForm goodsFormInfo = (GoodsForm) session.getAttribute("WEIXIN_GOODSFORM");

        if ("cart-1".equals(retWeixin)) {
            // 加入购物车
            model.addAttribute("list", goodsService.searchGoodsList(goodsForm));
            WeixinpayForm weixinpayForm = new WeixinpayForm();
            weixinpayForm = weixinCartService.searchWeixinPay(cartForm);

            if (weixinpayForm == null) {
                model.addAttribute("cartList", cartService.searchCartList(cartForm));
                model.addAttribute("message", "库存不够！");
                model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
                if ("201500000000030".equals(goodsFormInfo.getCommodityTypeId())) {
                    return "weixin/pointExchange/exchangeDetails";
                } else {
                    return "weixin/goods/details";
                }
            }

            result = cartService.addCart(cartForm);
            if (!result) {
                throw new SQLException("追加购物车失败！");
            }
            for(int i = 0; i < cartFormList.size(); i++){
                BigDecimal smallSumPrice=new BigDecimal(Double.toString(Double.valueOf(cartFormList.get(i).getCount())*Double.valueOf(cartFormList.get(i).getRetailPrice())));
                cartFormList.get(i).setSmallSumPrice(String.valueOf(smallSumPrice.setScale(2, BigDecimal.ROUND_HALF_UP)));
            }
            model.addAttribute("cartList", cartFormList);
            model.addAttribute("ret", retWeixin);
            return "weixin/cart/cart-1";
        } else if ("cart-2".equals(retWeixin)) {
            // 立即购买
            ReceiveForm receiveForm=new ReceiveForm();
            receiveForm.setGuestId(uvo.getGuestId());
            String addressDefault = guestService.searchAddressId(guestForm).getAddressId();
            model.addAttribute("addressDefault", addressDefault);
            List<ReceiveForm> list = receiveservice.searchlist(receiveForm);
            model.addAttribute("list", list);
            WeixinpayForm weixinPayForm = new WeixinpayForm();
            weixinPayForm = weixinCartService.searchWeixinPayImmediately(cartForm);
            model.addAttribute("alipayForm",weixinPayForm);
            if (weixinPayForm == null) {
                model.addAttribute("commodityType", commodityType);
                model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));
                model.addAttribute("cartList", cartFormList);
                model.addAttribute("message", "库存不够！");
                model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
                if ("201500000000030".equals(goodsForm.getCommodityTypeId())) {
                    return "weixin/pointExchange/exchangeDetails";
                } else {
                    return "weixin/goods/details";
                }
            }
            // 登陆后并且库存充足的场合，判定用户积分是否充足。
            String sumPoint = weixinPayForm.getSumPoint(); // 商品总积分
            // 用户剩余积分查询
            String guestPoint = guestService.searchGuestPoint(guestForm).getGuestPoint();
            // 换购商品积分  > 用户积分 的场合 ，并且积分兑换的场合
            if ("201500000000030".equals(goodsForm.getCommodityTypeId()) && Double.valueOf(sumPoint) > Double.valueOf(guestPoint)) {
                  model.addAttribute("commodityType", commodityType);
                    model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));
                    model.addAttribute("cartList", cartFormList);
                    model.addAttribute("message", "用户积分不足！");
                    model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
                    return "weixin/pointExchange/exchangeDetails";
            }
            model.addAttribute("cartList", cartFormList);
            model.addAttribute("ret", retWeixin);
            return "weixin/cart/cart-2";
        }
        return "error";
    }

    @RequestMapping(value = "weixinGuestLogin", method = RequestMethod.POST)
    public String guestLogin(Model model,
            HttpSession session,
            @Valid @ModelAttribute("guestForm")GuestForm guestForm,
            String ret,
            BindingResult result1,
            Device device) throws SQLException {
        log.info("微信客户绑定，验证客户信息，成功后进入系统");
        if(result1.hasErrors()) {
            return "weixin/login";
        }
        // 依据微信openId可以找到guestId
        GuestForm result = guestService.searchGuest(guestForm);
        GoodsForm goodsForm = new GoodsForm();
        List<GoodsForm> commodityType = goodsService.getType();
        model.addAttribute("goodsForm", goodsForm);
        model.addAttribute("commodityType", commodityType);

        if (result != null) {
            String openId = (String)session.getAttribute("WEIXIN_OPENID");
            result.setOpenId(openId);
            boolean addRet = weixinGuestService.updateGuestOpenId(result);
            if (!addRet) {
                model.addAttribute("message", "绑定微信用户失败");
                model.addAttribute("ret", ret);
                return "weixin/login";
            }
            UVO uvo = new UVO();
            uvo.setGuestId(result.getGuestId());
            uvo.setGuestName(result.getGuestName());
            uvo.setPassword(result.getPassword());
            uvo.setGender(result.getGender());
            uvo.setEmail(result.getEmail());
            uvo.setMobile(result.getMobile());
            uvo.setQq(result.getQq());
            uvo.setPhone(result.getPhone());
            uvo.setZip(result.getZip());
            uvo.setPoint(result.getPoint());
            session.setAttribute("UVO", uvo);
            CartForm cartForm = (CartForm) session.getAttribute("WEIXIN_CARTFORM");
            cartForm.setGuestId(uvo.getGuestId());
            //设置updateTime
            Date date = new Date();
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            cartForm.setUpdateTime(dateformat.format(date));
            //设置updateUser
            cartForm.setUpdateUser(uvo.getGuestName());
            List<CartForm> cartFormList = cartService.searchCartList(cartForm);
            GoodsForm goodsFormInfo = (GoodsForm) session.getAttribute("WEIXIN_GOODSFORM");

            if ("cart-1".equals(ret)) {
                // 加入购物车的逻辑
                model.addAttribute("list", goodsService.searchGoodsList(goodsForm));
                WeixinpayForm weixinpayForm = new WeixinpayForm();
                weixinpayForm = weixinCartService.searchWeixinPay(cartForm);

                if (weixinpayForm == null) {
                    model.addAttribute("cartList", cartFormList);
                    model.addAttribute("message", "库存不够！");
                    model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
                    if ("201500000000030".equals(goodsFormInfo.getCommodityTypeId())) {
                        return "weixin/pointExchange/exchangeDetails";
                    } else {
                        return "weixin/goods/details";
                    }
                }

                addRet = cartService.addCart(cartForm);
                if (!addRet) {
                    throw new SQLException("追加购物车失败！");
                }

                for(int i=0;i < cartFormList.size(); i++){
                    BigDecimal smallSumPrice = new BigDecimal(
                            Double.toString(Double.valueOf(cartFormList.get(i).getCount()) * Double.valueOf(cartFormList.get(i).getRetailPrice())));
                    cartFormList.get(i).setSmallSumPrice(String.valueOf(smallSumPrice.setScale(2, BigDecimal.ROUND_HALF_UP)));
                }
                model.addAttribute("ret", ret);
                model.addAttribute("cartList", cartFormList);
                return "weixin/cart/cart-1";
            } else if ("cart-2".equals(ret)) {
                // 立即购买
                ReceiveForm receiveForm=new ReceiveForm();
                receiveForm.setGuestId(uvo.getGuestId());
                String addressDefault = guestService.searchAddressId(result).getAddressId();
                model.addAttribute("addressDefault", addressDefault);
                List<ReceiveForm> list = receiveservice.searchlist(receiveForm);
                model.addAttribute("list", list);
                WeixinpayForm weixinPayForm = new WeixinpayForm();
                weixinPayForm = weixinCartService.searchWeixinPayImmediately(cartForm);
                model.addAttribute("alipayForm",weixinPayForm);
                if (weixinPayForm == null) {
                    model.addAttribute("commodityType", commodityType);
                    model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));
                    model.addAttribute("cartList", cartFormList);
                    model.addAttribute("message", "库存不够！");
                    model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
                    if ("201500000000030".equals(goodsForm.getCommodityTypeId())) {
                        return "weixin/pointExchange/exchangeDetails";
                    } else {
                        return "weixin/goods/details";
                    }
                }
                // 登陆后并且库存充足的场合，判定用户积分是否充足。
                String sumPoint = weixinPayForm.getSumPoint(); // 商品总积分
                // 用户剩余积分查询
                String guestPoint = guestService.searchGuestPoint(guestForm).getGuestPoint();
                // 换购商品积分  > 用户积分 的场合 ，并且积分兑换的场合
                if ("201500000000030".equals(goodsForm.getCommodityTypeId()) && Double.valueOf(sumPoint) > Double.valueOf(guestPoint)) {
                      model.addAttribute("commodityType", commodityType);
                        model.addAttribute("goodsForm", goodsService.searchGoods(goodsForm));
                        model.addAttribute("cartList", cartFormList);
                        model.addAttribute("message", "用户积分不足！");
                        model.addAttribute("orderlist", goodsService.searchGoodsListOrder(goodsForm));
                        return "weixin/pointExchange/exchangeDetails";
                }
                model.addAttribute("ret", ret);
                model.addAttribute("cartList", cartFormList);
                return "weixin/cart/cart-2";
            }
        } else {
            model.addAttribute("message", "用户名或密码错误！");
            List<CartForm> cartList = new ArrayList<>();
            model.addAttribute("cartList", cartList);
            model.addAttribute("ret", ret);
            return "weixin/login";
        }
        return "error";
    }
}
