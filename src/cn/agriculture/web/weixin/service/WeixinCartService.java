package cn.agriculture.web.weixin.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import cn.agriculture.common.util.UUIDUtil;
import cn.agriculture.web.form.AlipayForm;
import cn.agriculture.web.form.CartForm;
import cn.agriculture.web.form.WeixinpayForm;

@Service
@PropertySource("classpath:system.properties")
public class WeixinCartService {

    @Autowired
    QueryDAO queryDao;

    @Autowired
    UpdateDAO updateDao;

    @Autowired
    private Environment env;

    public WeixinpayForm searchWeixinPay(CartForm frm) {
        CartForm cartForm = queryDao.executeForObject("WeixinCart.selectGuestPrice", frm, CartForm.class);
        if (Double.valueOf(frm.getCount()) > Double.valueOf(cartForm.getCount())) {
            return null;
        }
        WeixinpayForm weixinpayForm = new WeixinpayForm();
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");

        weixinpayForm.setOutTradeNo(UUIDUtil.generateShortUuid() + dateformat.format(date));
        weixinpayForm.setSubject(frm.getGuestId() +"匿名购买的商品订单");
        Double price = Double.valueOf(frm.getCount()) * Double.valueOf(cartForm.getRetailPrice());
        // 不满100元加8元邮费
        String body;
        if(price < 100) {
            price = price + 8;
            weixinpayForm.setSubject(weixinpayForm.getSubject()+ "(由于本次订单未满100元，加收您邮费8元)");
            body = "品名：" + cartForm.getCommodityName() +", 数量："+ frm.getCount() +", 总价："+ price + "(由于本次订单未满100元，加收您邮费8元)";
        } else {
            body = "品名：" + cartForm.getCommodityName() +", 数量："+ frm.getCount() +", 总价："+ price;
        }
        weixinpayForm.setPrice(String.valueOf(price));
        weixinpayForm.setBody(body);
        String host = env.getProperty("host.web");
        weixinpayForm.setShowUrl(host + "/");
        weixinpayForm.setGuestId(frm.getGuestId());
        weixinpayForm.setCommodityId(cartForm.getCommodityId());
        weixinpayForm.setCount(frm.getCount());
        return weixinpayForm;
    }

    public boolean addWeixinpayHistory(WeixinpayForm frm) {
        // 做成支付宝历史单据，为了付款操作不成功时补款用。
        int result = updateDao.execute("WeixinCart.addWeixinHistory", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

    public WeixinpayForm searchWeixinPayImmediately(CartForm frm) {
        CartForm cartForm = queryDao.executeForObject("Cart.selectGuestPrice", frm, CartForm.class);
        if (Double.valueOf(frm.getCount()) > Double.valueOf(cartForm.getCount())) {
            return null;
        }
        WeixinpayForm weixinPayForm = new WeixinpayForm();
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        weixinPayForm.setOutTradeNo(frm.getGuestId() + dateformat.format(date));
        String body;
        Double price = Double.valueOf(frm.getCount()) * Double.valueOf(cartForm.getRetailPrice());
        // 商品类型='201500000000030'（积分兑换）的场合
        if ("201500000000030".equals(cartForm.getType())) {
            Double point = Double.valueOf(frm.getCount()) * Double.valueOf(cartForm.getPoint());
            body = "品名：" + cartForm.getCommodityName() +", 数量："+ frm.getCount() +", 花费总积分："+ point + "(积分兑换商品，免邮费)";
            weixinPayForm.setSubject(frm.getGuestId() +"立即积分兑换商品的订单" + "," + body );
            weixinPayForm.setPrice(String.valueOf("0.0")); // 由于积分兑换商品价格为"0.0"
        } else { // 积分兑换以外的场合
            weixinPayForm.setSubject(frm.getGuestId() +"立即购买的商品订单");
            // 不满100元加8元邮费
            if(price < 100) {
                price = price + 8;
                weixinPayForm.setSubject(weixinPayForm.getSubject()+ "(由于本次订单未满100元，加收您邮费8元)");
                body = "品名：" + cartForm.getCommodityName() +", 数量："+ frm.getCount() +", 总价："+ price + "(由于本次订单未满100元，加收您邮费8元)";
            } else {
                body = "品名：" + cartForm.getCommodityName() +", 数量："+ frm.getCount() +", 总价："+ price;
            }
            weixinPayForm.setPrice(String.valueOf(price));
        }

        // 设定商户订商品类型（积分兑换用）
        weixinPayForm.setOutTradeType(cartForm.getType());

        // 设定换购商品总积分（积分兑换用）
        if ("201500000000030".equals(cartForm.getType())) {
            Double sumPoint = Double.valueOf(frm.getCount()) * Double.valueOf(cartForm.getPoint());
            weixinPayForm.setSumPoint(String.valueOf(sumPoint)); // 设定换购商品总积分
        }
        weixinPayForm.setBody(body);
        String host = env.getProperty("host.web");
        weixinPayForm.setShowUrl(host + "/");
        weixinPayForm.setGuestId(frm.getGuestId());
        weixinPayForm.setCommodityId(cartForm.getCommodityId());
        weixinPayForm.setCount(frm.getCount());
        return weixinPayForm;
    }
    
    
    public List<AlipayForm> searchWeixinOrderList(CartForm frm, Integer index) {
        List<AlipayForm> result = queryDao.executeForObjectList("WeixinCart.selectWeixinPayHistoryList", frm, index*5, 5);
        return result;
    }
    
    public List<Integer> searchWeixinOrderListCount(CartForm frm) {
        Double count = queryDao.executeForObject("WeixinCart.selectWeixinPayHistoryListCount", frm, Double.class);
        List<Integer> list = new ArrayList<>();
        Integer pages = (int) Math.ceil(count/5);
        for (int i=1; i<=pages; i++) {
            list.add(i);
        }
        return list;
    }

    public int deleteWeixinOrder(AlipayForm frm) {
        int result = updateDao.execute("WeixinCart.deleteWeixinOrder", frm);
        return result;
    }
}
