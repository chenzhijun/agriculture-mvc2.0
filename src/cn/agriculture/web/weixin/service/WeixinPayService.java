package cn.agriculture.web.weixin.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import cn.agriculture.web.form.GuestForm;
import cn.agriculture.web.form.IntegralRuleForm;
import cn.agriculture.web.form.WeixinpayForm;

import com.mysql.jdbc.StringUtils;
import com.wxpay.utils.GetWxOrderno;
import com.wxpay.utils.RequestHandler;
import com.wxpay.utils.TenpayUtil;

@Service
@PropertySource("classpath:system.properties")
public class WeixinPayService {

    @Autowired
    QueryDAO queryDao;

    @Autowired
    UpdateDAO updateDao;

    @Autowired
    private Environment env;

    private String appid = "wx30a188c75aead17a";
    private String appsecret = "07a2f4d8241864c17d7c0b4dfe64efc9";
    private String partner = "1273298101";
    private String partnerkey = "MIICXwIBAAKBgQCp0MJrq7BR4ZQ5grKy";

    public boolean editStock(WeixinpayForm frm) {
        Integer stock = queryDao.executeForObject("WeixinPay.selectStock", frm, Integer.class);
        if (stock < Integer.valueOf(frm.getCount())) {
            return false;
        }
        int result = updateDao.execute("WeixinPay.editStock", frm);
        if (result == 1) {
            return true;
        }
        return false;
    }

    public boolean editPayment(Map<String, String> param) {
        WeixinpayForm weixinPayForm = new WeixinpayForm();
        weixinPayForm.setOutTradeNo(param.get("out_trade_no"));
        int result = updateDao.execute("WeixinPay.editPayment", weixinPayForm);
        if (result == 1) {
            // 根据客户花费钱数增加积分
            setIntegral(param);
            return true;
        }
        return false;
    }

    private void setIntegral(Map<String, String> param) {
        IntegralRuleForm ifrm = new IntegralRuleForm();
        ifrm.setType("下单");
        ifrm.setStatus("启用");
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        ifrm.setStartDate(dateformat.format(date));
        IntegralRuleForm ret =
                queryDao.executeForObject("IntegralRule.selectIntegralRule",
                                          ifrm,
                                          IntegralRuleForm.class);
        if (ret != null && !StringUtils.isNullOrEmpty(ret.getHowScore())) {
            WeixinpayForm weixinPayForm = new WeixinpayForm();
            weixinPayForm.setOutTradeNo(param.get("out_trade_no"));
            WeixinpayForm weixinPayHistory = queryDao.executeForObject("WeixinPay.selectPrice", weixinPayForm, WeixinpayForm.class);
            BigDecimal price = new BigDecimal(weixinPayHistory.getPrice());
            // 示例： 用户设置积分规则10元积5分，用户消费9元。
            // 四舍五入算法：5*9/10=4.5 结果积分为5。
            // 阶梯去尾算法：5*9/10=4.5 结果积分为4。
            // 满额积分算法：未满足10元，结果积分为0。
            BigDecimal howMoney = new BigDecimal(ret.getHowMoney());
            BigDecimal howScore = new BigDecimal(ret.getHowScore());
            BigDecimal point = new BigDecimal("0");
            //大于
            if (howMoney.compareTo(BigDecimal.ZERO) == 1) {
                if ("四舍五入".equals(ret.getHowSet())) {
                    point = price.multiply(howScore.divide(howMoney));
                    point = point.setScale(0,BigDecimal.ROUND_HALF_UP);
                } else if ("阶梯去尾".equals(ret.getHowSet())) {
                    point = price.multiply(howScore.divide(howMoney));
                    point = new BigDecimal(point.intValue());
                } else if ("满额积分".equals(ret.getHowSet())) {
                    point = howScore.multiply(new BigDecimal(price.divide(howMoney).intValue()));
                }
            }
            GuestForm guest = new GuestForm();
            guest.setGuestId(weixinPayHistory.getGuestId());
            GuestForm guestPointForm = queryDao.executeForObject("Guest.selectGuestPoint", guest, GuestForm.class);
            BigDecimal guestPoint = new BigDecimal(0);
            if (!StringUtils.isNullOrEmpty(guestPointForm.getGuestPoint())) {
                guestPoint = new BigDecimal(guestPointForm.getGuestPoint());
            }
            guest.setPoint((point.add(guestPoint)) + "");
            updateDao.execute("Guest.editGuestPoint", guest);
        }
    }

    @SuppressWarnings("static-access")
    public boolean getWxPayResult(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response , WeixinpayForm weixinPayForm) {
      String order_price = weixinPayForm.getPrice();
      // 接口中参数支付金额单位为【分】，参数值不能带小数.对账单中的交易金额单位为【元】。
      // 订单金额 单位元
       String price = order_price.toString().substring(0, order_price.toString().length()-2);
      // 金额转化为分为单位
      float sessionmoney = Float.parseFloat(price);
      String finalmoney = String.format("%.2f", sessionmoney);
      finalmoney = finalmoney.replace(".", "");
      // openid取得
      String openid = (String) session.getAttribute("WEIXIN_OPENID");
      // 微信支付分配的商户号
      String mch_id = partner;
      // 商品或支付单简要描述
      String body = weixinPayForm.getBody();
      // 生成随机数
      String currTime = TenpayUtil.getCurrTime();
      String strTime = currTime.substring(8, currTime.length());
      String strRandom = TenpayUtil.buildRandom(4) + "";
      String nonce_str = strTime + strRandom;
      // 附加数据
      String attach = "" ;
      // 商户订单号
      String out_trade_no = weixinPayForm.getOutTradeNo();
      // 总金额
      int total_fee = Integer.parseInt(finalmoney);
      // 订单生成的机器 IP
      String spbill_create_ip = request.getRemoteAddr();
      // 接收微信支付异步通知回调地址
      String host = env.getProperty("host.web");
      String notify_url = host + "/initWeixinPayBack";
      // 交易类型
      String trade_type = "JSAPI";

      SortedMap<String, String> packageParams = new TreeMap<String, String>();
      packageParams.put("appid", appid);
      packageParams.put("mch_id", mch_id);
      packageParams.put("nonce_str", nonce_str);
      packageParams.put("body", body);
      packageParams.put("out_trade_no", out_trade_no);
      packageParams.put("total_fee", finalmoney);
      packageParams.put("spbill_create_ip", spbill_create_ip);
      packageParams.put("notify_url", notify_url);
      packageParams.put("trade_type", trade_type);
      packageParams.put("openid", openid);

      RequestHandler reqHandler = new RequestHandler(request, response);
      reqHandler.init(appid, appsecret, partnerkey);
      String sign = reqHandler.createSign(packageParams);
      String xml="<xml>"+
              "<appid>"+appid+"</appid>"+
              "<mch_id>"+mch_id+"</mch_id>"+
              "<nonce_str>"+nonce_str+"</nonce_str>"+
              "<sign>"+sign+"</sign>"+
              "<body>"+body+"</body>"+
              "<attach>"+attach+"</attach>"+
              "<out_trade_no>"+out_trade_no+"</out_trade_no>"+
              "<total_fee>"+total_fee+"</total_fee>"+
              "<spbill_create_ip>"+spbill_create_ip+"</spbill_create_ip>"+
              "<notify_url>"+notify_url+"</notify_url>"+
              "<trade_type>"+trade_type+"</trade_type>"+
              "<openid>"+openid+"</openid>"+
              "</xml>";
      String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
      String prepay_id="";
      try {
          prepay_id = new GetWxOrderno().getPayNo(createOrderURL, xml);
          if(StringUtils.isNullOrEmpty(prepay_id)){
              return false;
          }
      } catch (Exception e1) {
          e1.printStackTrace();
      }
      SortedMap<String, String> finalpackage = new TreeMap<String, String>();
      String timestamp = Long.toString(new Date().getTime());
      String nonceStr2 = nonce_str;
      String prepay_id2 = "prepay_id="+prepay_id;
      String packages = prepay_id2;
      finalpackage.put("appId", appid);
      finalpackage.put("timeStamp", timestamp);
      finalpackage.put("nonceStr", nonceStr2);
      finalpackage.put("package", packages);
      finalpackage.put("signType", "MD5");
      // 将参数签名
      String finalsign = reqHandler.createSign(finalpackage);
      model.addAttribute("appid", appid);
      model.addAttribute("timeStamp", timestamp);
      model.addAttribute("nonceStr", nonceStr2);
      model.addAttribute("packages", packages);
      model.addAttribute("sign", finalsign);

      return true;
    }

}
